# Descomplica

Projetos de exemplo para as aulas da faculdade Descomplica

## Módulo de front-end - React

Neste respositório você encontrará um projeto já pronto! Você só terá que fazer algumas adaptações caso você altere alguma informação como os endereços de API, se você seguiu todo o passo-a-passo para subir as APIs em Java SpringBoot e Python da forma como está neste repositório, então provavelmente você não precisará alterar nada!

Teremos os seguintes passos:

1) Download do pacote de desenvolvimento Node.js e npm
2) Importação do projeto React deste repositório
3) Como criar uma imagem do Docker à partir do projeto em React
4) Como subir um container para nosso site React no nosso Docker

## Download do Node.js e npm

Bom, a primeira coisa é fazer o download do Node.js, neste caso é um pacote de desenvolvimento que nos permite programar e compilar aplicações Javascript.

[https://nodejs.org/en/download/](https://nodejs.org/en/download/)

No instalador, certifique-se que o item "npm package manager" esteja como um item marcado para instalação, deverá estar conforme a imagem abaixo:

![Instalador Node.js e npm](./imagens/imagem106.png "imagem106")

